# Pull base image
From tomcat:8-jre11

# Maintainer
MAINTAINER "sapient"

# copy war file on to container
COPY ./app.war /usr/local/tomcat/webapps